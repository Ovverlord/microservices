package app.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class Adapter {

    @Value("${url}")
    private String URL;

    private final String ENDPOINT = "/consumer";

    private final RestTemplate restTemplate;

    public Adapter() {
        restTemplate = new RestTemplate();
    }

    public void sendRequest(Integer callTimes) {
        for(int i =0;i<callTimes;i++){
            restTemplate.postForObject(URL + ENDPOINT,null, Void.class);
        }

    }
}