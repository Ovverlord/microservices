package app.controller;

import app.service.Adapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class ProducerController {

    @Autowired
    private Adapter adapter;

    @PostMapping("/producer")
    @ResponseStatus(value = HttpStatus.OK)
    public void postProducer(@RequestParam(value="callTimes") Integer callTimes) {
        adapter.sendRequest(callTimes);
    }
}