package app.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.atLeastOnce;

public class AdapterTest {

    private RestTemplate restTemplate = Mockito.mock(RestTemplate.class);

    @Value("${url}")
    private String URL;

    private String ENDPOINT;

    @Before
    public void setUp(){
        ENDPOINT = "/consumer";
    }

    @Test
    public void sendRequestTest(){
        restTemplate.postForObject(URL + ENDPOINT,null, Void.class);
        Mockito.verify(restTemplate, Mockito.times(1))
                .postForObject(URL + ENDPOINT,null, Void.class);

        Mockito.verify(restTemplate, atLeastOnce()).postForObject(URL + ENDPOINT,null, Void.class);
    }

}