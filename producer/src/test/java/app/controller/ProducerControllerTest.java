package app.controller;


import app.service.Adapter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProducerController.class)
public class ProducerControllerTest  {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    Adapter adapter;

    @Test
    public void postProducerTest() throws Exception {
        mockMvc.perform(post("/producer?callTimes=1"))
                .andExpect(status().isOk());
    }
}
