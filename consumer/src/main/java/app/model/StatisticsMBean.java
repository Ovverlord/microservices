package app.model;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public interface StatisticsMBean {
    void putValue(String key, AtomicLong value);
    Map getStatisticsMap();
}
