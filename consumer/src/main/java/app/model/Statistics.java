package app.model;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class Statistics implements StatisticsMBean {

    private Map<String, AtomicLong> statisticsMap = new HashMap<>();

    @Override
    public void putValue(String key, AtomicLong value){
        statisticsMap.putIfAbsent(key, new AtomicLong(0));
        statisticsMap.put(key, value);
    }

    @Override
    public Map getStatisticsMap() {
        return statisticsMap;
    }
}
