package app.controller;

import app.model.Statistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;

@Controller
public class ConsumerController {

    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private Statistics statistics;

    @PostMapping("/consumer")
    @ResponseStatus(value = HttpStatus.OK)
    public void postConsumer(HttpServletRequest request){

        System.out.println(request.getRemoteAddr());
        counter.incrementAndGet();
        statistics.putValue(request.getRemoteAddr(),counter);
    }

    @GetMapping("/consumer")
    public String getConsumer(Model model){

        model.addAttribute("statistics",statistics.getStatisticsMap());

        return "mainConsumer";
    }
}
